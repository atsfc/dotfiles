if command -v zoxide > /dev/null
  zoxide init fish | source
end

if command -v starship > /dev/null
  starship init fish | source
end

if test ! -e $HOME/.local/bin
  mkdir -p $HOME/.local/bin
else
  fish_add_path -ag $HOME/.local/bin
end

# if test -e /usr/local/go
#   fish_add_path -ag /usr/local/go/bin
# end

sed -i.bak 's|fd_opts --color=always|fd_opts --hidden --exclude=.git --color=always|' $__fish_config_dir/functions/_fzf_search_directory.fish

function wait_and_detect_sudo
  while true
    set -l last_command (history -1)
    if string match --regex "sudo .*" $last_command
      echo "a command run with sudo."
      break
    end
    sleep 1  # Adjust the sleep interval as needed
  end
end

function fish_job_bg
  while true
    sleep 1 
    fish -c 'wait_and_detect_sudo' &
  end
end

# fish_job_bg

set -xg FZF_DEFAULT_COMMAND 'fd --type file --follow --color=always --hidden --exclude .git'
set -xg FZF_DEFAULT_OPTS '--ansi --layout=reverse'

function cp
  command gcp $argv
end

## A ls function with fancy coloring.
function ll
  eza --long --sort=modified --icons $argv
end

function lla
  eza --long --all --sort=modified --icons $argv
end

function ch -d 'Cheat sheet parser.'
  curl -s cheat.sh/$argv | bat --paging=never
end

function short -d 'short url expander'
  set output (curl -L\# "https://urlex.org/$argv" | grep -io '<a href=".*" rel="external nofollow">' | sed 's/<a href="\(.*\)" rel="external nofollow">/\1/g')
  set out (string split "?" $output)
  echo $out[1]
end

function yt -d 'a commandline youtube player'
  mpv --no-video --save-position-on-quit --ao=pulse --term-playing-msg="Title: \${media-title}" "$argv"
end

function vpn
  dpkg --get-selections | grep protonvpn-cli > /dev/null
  if not command -v protonvpn-cli > /dev/null
    echo 'protonvpn-cli is not installed!'
  else
    if test -z "$argv"
      warp-cli disconnect > /dev/null
      if test $status -eq 0
        echo 'WARP is disconnected.'
        protonvpn-cli c -f
        if test $status -eq 0
          protonvpn-cli s
        end
      end
    else if test "$argv" = "p"
      warp-cli disconnect > /dev/null
      if test $status -eq 0
        echo 'WARP is disconnected.'
        protonvpn-cli c --p2p
        if test $status -eq 0
          protonvpn-cli s
        end
      end
    else
      if test "$argv[1]" = "d"
        protonvpn-cli d
        if test $status -eq 0
          warp-cli connect > /dev/null
          if test $status -eq 0
            echo 'WARP is connected.'
          end
        end
      end
    end
  end
end

function fish_greeting
end

function multicd
  echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
end

function last_history_item; echo $history[1]; end

function commandpath
  set -l cmd (string replace -r '^=(.+)$' '$1' -- $argv)
  or return
  command -s $cmd
end

fish_vi_key_bindings

if test -e ~/anaconda3/etc/fish/conf.d/conda.fish
  source ~/anaconda3/etc/fish/conf.d/conda.fish
  conda config --set changeps1 False
end

if test -e /mnt/depo/anaconda3/etc/fish/conf.d/conda.fish
  source /mnt/depo/anaconda3/etc/fish/conf.d/conda.fish
  conda config --set changeps1 False
end

if test -e ~/.local/bin/rga
  function rga-fzf
    set RG_PREFIX 'rga --files-with-matches'
    if test (count $argv) -gt 1
        set RG_PREFIX "$RG_PREFIX $argv[1..-2]"
    end
    set -l file $file
    set file (
        FZF_DEFAULT_COMMAND="$RG_PREFIX '$argv[-1]'" \
        fzf --sort \
            --preview='test ! -z {} && \
                rga --pretty --context 5 {q} {}' \
            --phony -q "$argv[-1]" \
            --bind "change:reload:$RG_PREFIX {q}" \
            --preview-window='50%:wrap'
    ) && \
    echo "opening $file" && \
    open "$file"
  end
end

function pdfoptimizer
  set -l outfile (echo $argv | sed 's/\(.*\).pdf/\\1_optimized.pdf/g')
  command gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
    -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH \
    -dDetectDuplicateImages -dCompressFonts=true -r150 \
    -sOutputFile=$outfile $argv
end


abbr --add --position anywhere --regex '=.*' --function commandpath commandpath
abbr -a !! --position anywhere --function last_history_item
abbr --add dotdot --regex '^\.\.+$' --function multicd
abbr y 'yazi'
abbr gp 'git pull'
abbr gc 'git commit -m'
abbr ga 'git add'
abbr gs 'git show'

switch $__os_release
  case 'Android'
    abbr ua 'apt update && apt full-upgrade'
    abbr i 'apt install'
    abbr r 'apt remove --purge'
    abbr aur 'apt autoremove --purge'
    abbr s 'apt search'
    abbr di 'dpkg -i'
    abbr rr 'sudo reboot'
  case 'Pop'
    abbr ua 'sudo apt update && sudo apt full-upgrade ; flatpak update'
    abbr i 'sudo apt install'
    abbr r 'sudo apt remove --purge'
    abbr aur 'sudo apt autoremove --purge'
    abbr s 'apt search'
    abbr fs 'flatpak search'
    abbr fi 'flatpak install'
    abbr fr 'flatpak remove'
    abbr di 'sudo dpkg -i'
    abbr rr 'reboot --reboot'
    abbr ssk 'kitty +kitten ssh -o TCPKeepAlive=yes -o ServerAliveCountMax=20 -o ServerAliveInterval=15'
  case '*'
    abbr ua 'sudo apt update && sudo apt full-upgrade'
    abbr i 'sudo apt install'
    abbr r 'sudo apt remove --purge'
    abbr aur 'sudo apt autoremove --purge'
    abbr s 'apt search'
    abbr di 'sudo dpkg -i'
    abbr rr 'sudo reboot'
end

# commandline colors:old school
set -U fish_color_normal normal
set -U fish_color_command 00FF00
set -U fish_color_quote 44FF44
set -U fish_color_redirection 7BFF7B
set -U fish_color_end FF7B7B
set -U fish_color_error A40000
set -U fish_color_param 30BE30
set -U fish_color_comment 30BE30
set -U fish_color_match --background=brblue
set -U fish_color_selection white --bold --background=brblack
set -U fish_color_search_match bryellow --background=brblack
set -U fish_color_history_current --bold
set -U fish_color_operator 00a6b2
set -U fish_color_escape 00a6b2
set -U fish_color_cwd green
set -U fish_color_cwd_root red
set -U fish_color_valid_path --underline
set -U fish_color_autosuggestion 777777
set -U fish_color_user brgreen
set -U fish_color_host normal
set -U fish_color_cancel --reverse
set -U fish_pager_color_prefix normal --bold --underline
set -U fish_pager_color_progress brwhite --background=cyan
set -U fish_pager_color_completion normal
set -U fish_pager_color_description B3A06D
set -U fish_pager_color_selected_background --background=brblack
