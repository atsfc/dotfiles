filetype indent plugin on
syntax on
set mouse=a
set number
set clipboard=unnamedplus
" set spell spelllang=en_us

" An auto installer for vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!echo Installing vim-plug plugin manager.'
  silent execute '!curl -sfLo ' . data_dir . '/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim > /dev/null'
  silent execute '!echo The vim-plug plugin manager is installed.'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" This is a Vim/Neovim plugin that provides Fish syntax highlighting,
" automatic indentation as you type, file detection, and a colorscheme
" matching Fish's default shell highlighting.
Plug 'nickeb96/fish.vim'

" A collection of language packs for Vim.
Plug 'sheerun/vim-polyglot'

" Kitty terminal config vim highlighting
Plug 'fladson/vim-kitty'

" A vim autocompletion plugin
" build-essential, cmake, python3-dev required
" sudo apt install build-essential cmake python3-dev
Plug 'ycm-core/YouCompleteMe', { 'do': './install.py --clangd-completer' }

" Add vim fzf funcionality
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Autocompletes paranthesis.
Plug 'jiangmiao/auto-pairs'

" Initialize plugin system
call plug#end()
