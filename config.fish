if test (uname -o) = 'Android'
  set __os_release 'Android'
else
  set __os_release (lsb_release -is)
end

switch $__os_release
  case 'Pop'
    set -l _package_01 curl curl
    set -l _package_02 exa exa
    set -l _package_03 fdfind fd-find
    set -l _package_04 bat bat
    set -l _package_05 lua lua5.4
    set -l _package_06 fzf fzf
    set -l _package_07 git git
    set -l _package_08 grc grc
    set -l _package_09 gs ghostscript
    set -l _package_10 gcp gcp
    
    set _required_packages $_package_01 $_package_02 $_package_03 $_package_04 $_package_05 $_package_06 $_package_07 $_package_08 $_package_09 $_package_10

  case 'Ubuntu'
    set -l _package_01 curl curl
    set -l _package_02 exa exa
    set -l _package_03 fdfind fd-find
    set -l _package_04 batcat bat
    set -l _package_05 lua lua5.4
    set -l _package_06 fzf fzf
    set -l _package_07 git git
    set -l _package_08 grc grc
    set -l _package_09 gs ghostscript
    set -l _package_10 gcp gcp
    
    set _required_packages $_package_01 $_package_02 $_package_03 $_package_04 $_package_05 $_package_06 $_package_07 $_package_08 $_package_09 $_package_10

  case 'Debian'
    set -l _package_01 curl curl
    set -l _package_02 exa exa
    set -l _package_03 fdfind fd-find
    set -l _package_04 batcat bat
    set -l _package_05 lua lua5.4
    set -l _package_06 fzf fzf
    set -l _package_07 git git
    set -l _package_08 grc grc
    set -l _package_09 gcp gcp

    set _required_packages $_package_01 $_package_02 $_package_03 $_package_04 $_package_05 $_package_06 $_package_07 $_package_08 $_package_09

  case 'Android'
    set -l _package_01 curl curl
    set -l _package_02 exa exa
    set -l _package_03 fd fd
    set -l _package_04 bat bat
    set -l _package_05 lua lua54
    set -l _package_06 fzf fzf
    set -l _package_07 git git
    set -l _package_08 ffmpeg ffmpeg
    set -l _package_09 mpv mpv
    set -l _package_10 gs ghostscript

    set _required_packages $_package_01 $_package_02 $_package_03 $_package_04 $_package_05 $_package_06 $_package_07 $_package_08 $_package_09 $_package_10

  case '*'
    echo (set_color red)Your distribution is not supported.(set_color normal)
end

function __error_handler -a stat -a package
  if test $stat -eq 0
    echo The package (set_color red)$package(set_color normal) is installed.
    return 0
  else
    echo A (set_color red)problem(set_color normal) occured when installing the package (set_color red)$package(set_color normal).
    return $stat
  end
end

function _required_package -a i
  if command -v $_required_packages[$i] > /dev/null
    return 0
  else
    echo (string repeat -n 70 '-')
    echo Installing required package (set_color red)$_required_packages[(math $i+1)](set_color normal).
    if test $__os_release = 'Android'
      apt-get install $_required_packages[(math $i+1)] -yy
      __error_handler $status $_required_packages[(math $i+1)]
    else
      sudo apt-get install $_required_packages[(math $i+1)] -yy
      __error_handler $status $_required_packages[(math $i+1)]
    end
    echo (string repeat -n 70 '-')
  end
end

set -l __counter1 0
for i in (seq 1 2 (count $_required_packages))
  _required_package $i
  set -l __counter1 (math $__counter1 + $status)
end

function _zoxide_checker
  if command -v zoxide > /dev/null
    zoxide init fish | source
  else
    echo (string repeat -n 70 '-')
    echo Installing required package (set_color red)zoxide(set_color normal).
    if test $__os_release = 'Android'
      apt-get install zoxide -yy
    else
      curl -sS 'https://raw.githubusercontent.com/ajeetdsouza/zoxide/main/install.sh' | bash
    end
    __error_handler $status zoxide
    echo (string repeat -n 70 '-')
  end
end

function _starship_checker
  if command -v starship > /dev/null
    if test ! -e $HOME/.config/starship.toml 
      curl -L\# 'https://gitlab.com/atsfc/dotfiles/-/raw/main/starship.toml' -o $HOME/.config/starship.toml
    end
    starship init fish | source
  else
    echo (string repeat -n 70 '-')
    echo Installing required package (set_color red)starship(set_color normal).
    if test $__os_release = 'Android'
      apt-get install starship -yy
    else
      curl -sS https://starship.rs/install.sh | sh
    end
    __error_handler $status starship
    echo (string repeat -n 70 '-')
  end
end
if test ! -e $HOME/.local/bin
  mkdir -p $HOME/.local/bin
else
  fish_add_path -ag $HOME/.local/bin
end

if test -e /usr/local/go
  fish_add_path -ag /usr/local/go/bin
end

switch $__os_release
  case 'Ubuntu'
    if test ! -e $HOME/.local/bin/bat
      ln -s /usr/bin/batcat $HOME/.local/bin/bat
    end
    if test ! -e $HOME/.local/bin/fd
      ln -s /usr/bin/fdfind $HOME/.local/bin/fd
    end
  case 'Debian'
    if test ! -e $HOME/.local/bin/bat
      ln -s /usr/bin/batcat $HOME/.local/bin/bat
    end
    if test ! -e $HOME/.local/bin/fd
      ln -s /usr/bin/fdfind $HOME/.local/bin/fd
    end
  case 'Pop'
    if test ! -e $HOME/.local/bin/fd
      ln -s /usr/bin/fdfind $HOME/.local/bin/fd
    end
end

switch $__os_release
  case 'Android'
    set -l _addon_01 'fzf_configure_bindings.fish' 'fzf.fish' 'https://github.com/PatrickF1/fzf.fish.git'
    set -l _addon_02 '_autopair_backspace.fish' 'autopair.fish' 'https://github.com/jorgebucaran/autopair.fish.git'
    set -l _addon_03 'man.fish' 'colored_man_pages.fish' 'https://github.com/PatrickF1/colored_man_pages.fish.git'

    set _required_addons $_addon_01 $_addon_02 $_addon_03
  case '*'
    set -l _addon_01 'fzf_configure_bindings.fish' 'fzf.fish' 'https://github.com/PatrickF1/fzf.fish.git'
    set -l _addon_02 '_autopair_backspace.fish' 'autopair.fish' 'https://github.com/jorgebucaran/autopair.fish.git'
    set -l _addon_03 'man.fish' 'colored_man_pages.fish' 'https://github.com/PatrickF1/colored_man_pages.fish.git'
    set -l _addon_04 'grc.wrap.fish' 'plugin-grc' 'https://github.com/oh-my-fish/plugin-grc.git'
    
    set _required_addons $_addon_01 $_addon_02 $_addon_03 $_addon_04 
end

function _addon_checker -a i -d 'Checks if the addons installed. Otherwise install them from git.'
  if test -e $__fish_config_dir/functions/$_required_addons[$i]
    return 0
  else
    echo (string repeat -n 70 '-')
    echo Installing add-on (set_color red)$_required_addons[(math $i+1)](set_color normal).
    if test $__os_release = 'Android'
      set tmpdir $HOME/tmpdir
      mkdir -p $tmpdir
    else
      set tmpdir /tmp
    end
    git clone $_required_addons[(math $i+2)] $tmpdir/$_required_addons[(math $i+1)] 
    if test $status -eq 0
      set -l _check_dirs functions completions conf.d
      set _counter 0
      for dirs in $_check_dirs
        if test -d $tmpdir/$_required_addons[(math $i+1)]/$dirs
          cp -r $tmpdir/$_required_addons[(math $i+1)]/$dirs/* $__fish_config_dir/$dirs > /dev/null
          set _counter (math $_counter + $status)
        end
      end
      if test $_counter -eq 0
        echo (set_color red)$_required_addons[(math $i+1)](set_color normal)" is installed."
        echo (string repeat -n 70 '-')
        if test $__os_release = 'Android'
          rm -rf $tmpdir
          return 0
        else
          rm -rf $tmpdir/$_required_addons[(math $i+1)]
          return 0
        end
      else
        echo There is a problem when installing (set_color red)$_required_addons[(math $i+1)](set_color normal).
        return 1
      end
    else
      echo There is a problem with (set_color red)git(set_color normal).
      return 1
    end
  end
end

if test ! -e $HOME/.local/bin/yt-dlp
  echo (string repeat -n 70 '-')
  echo Installing (set_color red)yt-dlp(set_color normal).
  curl -L\# https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o $HOME/.local/bin/yt-dlp
  __error_handler $status yt-dlp
  chmod a+rx $HOME/.local/bin/yt-dlp
end

set -l __counter2 0
for i in (seq 1 3 (count $_required_addons))
  _addon_checker $i
  set -l __counter2 (math $__counter2 + $status)
end

sed -i.bak 's|fd_opts --color=always|fd_opts --hidden --exclude=.git --color=always|' $__fish_config_dir/functions/_fzf_search_directory.fish

if test $__os_release = 'Android'
  set _font_name font
else
  set _font_name 'Sauce Code Pro Nerd Font Complete Mono'
end
set _font_archive_name SourceCodePro

if test $__os_release = 'Android'
  set _font_dir $HOME/.termux
else
  set _font_dir $HOME/.local/share/fonts 
end

function _font_checker -d 'Checks if the required font is installed.'
  if test -e $_font_dir/$_font_name.ttf
    return 0
  else
    echo (string repeat -n 70 '-')
    echo Installing required (set_color red)fonts(set_color normal).
    curl -\#fLo $_font_dir/$_font_archive_name.zip --create-dirs https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/$_font_archive_name.zip # > /dev/null
    if test $status -eq 0
      unzip $_font_dir/$_font_archive_name.zip -d $_font_dir/$_font_archive_name > /dev/null
      if test $status -eq 0
        rm $_font_dir/$_font_archive_name.zip
        if test $__os_release = 'Android'
          cp $_font_dir/$_font_archive_name/'Sauce Code Pro Semibold Nerd Font Complete'.ttf $_font_dir/$_font_name.ttf
        else
          cp $_font_dir/$_font_archive_name/* $_font_dir/
          fc-cache -f $_font_dir > /dev/null
        end
        if test $status -eq 0
          rm -rf $_font_dir/$_font_archive_name
          echo (set_color red)Fonts(set_color normal) are installed.
          echo Now you can set your terminal emulator to use the font (set_color red)$_font_name.(set_color normal)
          echo You may need to restart your terminal emulator to reach updated font database.
          echo (string repeat -n 70 '-')
          return 0
        else
          echo There is a problem about fc-cache command.
          return 1
        end
      else
        echo There is a problem about unzip command.
        return 1
      end
      echo (string repeat -n 70 '-')
      return 0 
    else
      echo There is a problem about downloading the font.
      return 1
    end 
  end
end

_font_checker
set -l __counter3 $status

_zoxide_checker
set -l __counter4 $status

_starship_checker
set -l __counter5 $status

set -l __counter (math $__counter1+$__counter2+$__counter3+$__counter4+$__counter5)

if test $__counter -ne 0
  echo There is a problem overall installation process. Please see the config.fish file.
else
  ########### overrides ###########
  
  function wait_and_detect_sudo
    while true
      set -l last_command (history -1)
      if string match --regex "sudo .*" $last_command
        echo "a command run with sudo."
        break
      end
      sleep 1  # Adjust the sleep interval as needed
    end
  end

  function fish_job_bg
    while true
      sleep 1 
      fish -c 'wait_and_detect_sudo' &
    end
  end

  # fish_job_bg

  set -xg FZF_DEFAULT_COMMAND 'fd --type file --follow --color=always --hidden --exclude .git'
  set -xg FZF_DEFAULT_OPTS '--ansi --layout=reverse'
  
  switch $__os_release
    case 'Android'
    case '*'
    ## A copy function which uses fancy progressbar
    function cp
      command gcp $argv
    end
  end

  ## A ls function with fancy coloring.
  function ll
    dpkg --get-selections | grep exa > /dev/null
    if test $status -ne 0
      echo 'exa is not installed!'
    else
      exa --long --sort=modified --icons $argv
    end
  end

  function lla
    dpkg --get-selections | grep exa > /dev/null
    if test $status -ne 0
      echo 'exa is not installed!'
    else
      exa --long --all --sort=modified --icons $argv
    end
  end

  function ch -d 'Cheat sheet parser.'
    curl -s cheat.sh/$argv | bat --paging=never
  end

  function short -d 'short url expander'
    set output (curl -L\# "https://urlex.org/$argv" | grep -io '<a href=".*" rel="external nofollow">' | sed 's/<a href="\(.*\)" rel="external nofollow">/\1/g')
    set out (string split "?" $output)
    echo $out[1]
  end

  function yt -d 'a commandline youtube player'
    mpv --no-video --save-position-on-quit --ao=pulse --term-playing-msg="Title: \${media-title}" "$argv"
  end

  function vpn
    dpkg --get-selections | grep protonvpn-cli > /dev/null
    if test $status -ne 0
      echo 'protonvpn-cli is not installed!'
    else
      if test -z "$argv"
        warp-cli disconnect > /dev/null
        if test $status -eq 0
          echo 'WARP is disconnected.'
          protonvpn-cli c -f
          if test $status -eq 0
            protonvpn-cli s
          end
        end
      else if test "$argv" = "p"
        warp-cli disconnect > /dev/null
        if test $status -eq 0
          echo 'WARP is disconnected.'
          protonvpn-cli c --p2p
          if test $status -eq 0
            protonvpn-cli s
          end
        end
      else
        if test "$argv[1]" = "d"
          protonvpn-cli d
          if test $status -eq 0
            warp-cli connect > /dev/null
            if test $status -eq 0
              echo 'WARP is connected.'
            end
          end
        end
      end
    end
  end

  function fish_greeting
  end

  function multicd
    echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
  end
  
  function last_history_item; echo $history[1]; end
  
  function commandpath
    set -l cmd (string replace -r '^=(.+)$' '$1' -- $argv)
    or return
    command -s $cmd
  end

  fish_vi_key_bindings

  if test -e ~/anaconda3/etc/fish/conf.d/conda.fish
    source ~/anaconda3/etc/fish/conf.d/conda.fish
    conda config --set changeps1 False
  end
  
  if test -e /mnt/depo/anaconda3/etc/fish/conf.d/conda.fish
    source /mnt/depo/anaconda3/etc/fish/conf.d/conda.fish
    conda config --set changeps1 False
  end

  if test -e ~/.local/bin/rga
    function rga-fzf
      set RG_PREFIX 'rga --files-with-matches'
      if test (count $argv) -gt 1
          set RG_PREFIX "$RG_PREFIX $argv[1..-2]"
      end
      set -l file $file
      set file (
          FZF_DEFAULT_COMMAND="$RG_PREFIX '$argv[-1]'" \
          fzf --sort \
              --preview='test ! -z {} && \
                  rga --pretty --context 5 {q} {}' \
              --phony -q "$argv[-1]" \
              --bind "change:reload:$RG_PREFIX {q}" \
              --preview-window='50%:wrap'
      ) && \
      echo "opening $file" && \
      open "$file"
    end
  end
  
  function pdfoptimizer
    set -l outfile (echo $argv | sed 's/\(.*\).pdf/\\1_optimized.pdf/g')
    command gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
      -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH \
      -dDetectDuplicateImages -dCompressFonts=true -r150 \
      -sOutputFile=$outfile $argv
  end


  abbr --add --position anywhere --regex '=.*' --function commandpath commandpath
  abbr -a !! --position anywhere --function last_history_item
  abbr --add dotdot --regex '^\.\.+$' --function multicd
  abbr y 'yazi'
  abbr gp 'git pull'
  abbr gc 'git commit -m'
  abbr ga 'git add'
  abbr gs 'git show'
  abbr wt 'wget -q --show-progress'

  switch $__os_release
    case 'Android'
      abbr ua 'apt update && apt full-upgrade'
      abbr i 'apt install'
      abbr r 'apt remove --purge'
      abbr aur 'apt autoremove --purge'
      abbr s 'apt search'
      abbr di 'dpkg -i'
      abbr rr 'sudo reboot'
    case 'Pop'
      abbr ua 'sudo apt update && sudo apt full-upgrade ; flatpak update'
      abbr i 'sudo apt install'
      abbr r 'sudo apt remove --purge'
      abbr aur 'sudo apt autoremove --purge'
      abbr s 'apt search'
      abbr fs 'flatpak search'
      abbr fi 'flatpak install'
      abbr fr 'flatpak remove'
      abbr di 'sudo dpkg -i'
      abbr rr 'reboot --reboot'
      abbr ssk 'kitty +kitten ssh -o TCPKeepAlive=yes -o ServerAliveCountMax=20 -o ServerAliveInterval=15'
    case '*'
      abbr ua 'sudo apt update && sudo apt full-upgrade'
      abbr i 'sudo apt install'
      abbr r 'sudo apt remove --purge'
      abbr aur 'sudo apt autoremove --purge'
      abbr s 'apt search'
      abbr di 'sudo dpkg -i'
      abbr rr 'sudo reboot'
  end

  # commandline colors:old school
  set -U fish_color_normal normal
  set -U fish_color_command 00FF00
  set -U fish_color_quote 44FF44
  set -U fish_color_redirection 7BFF7B
  set -U fish_color_end FF7B7B
  set -U fish_color_error A40000
  set -U fish_color_param 30BE30
  set -U fish_color_comment 30BE30
  set -U fish_color_match --background=brblue
  set -U fish_color_selection white --bold --background=brblack
  set -U fish_color_search_match bryellow --background=brblack
  set -U fish_color_history_current --bold
  set -U fish_color_operator 00a6b2
  set -U fish_color_escape 00a6b2
  set -U fish_color_cwd green
  set -U fish_color_cwd_root red
  set -U fish_color_valid_path --underline
  set -U fish_color_autosuggestion 777777
  set -U fish_color_user brgreen
  set -U fish_color_host normal
  set -U fish_color_cancel --reverse
  set -U fish_pager_color_prefix normal --bold --underline
  set -U fish_pager_color_progress brwhite --background=cyan
  set -U fish_pager_color_completion normal
  set -U fish_pager_color_description B3A06D
  set -U fish_pager_color_selected_background --background=brblack
end
