# Some automated configuration files for vim and fish shell on Ubuntu based distributions.

I made some fancy colored stuff with automated installs. You are welcome to use the configuration files. I tested these on Ubuntu 22.04 and Pop_OS! 22.04.

#### For vim;

- put `.vimrc` file to your `$HOME` directory.
  
  ###### Properties/Plugins
  
  - indent plugin on.
  
  - syntax coloring on.
  
  - line numbers on.
  
  - you can enable spellchecker. It's commented on the file.
  
  - automatically installs Plug plugin manager. For this, it needs to install `curl`.
  
  - installs [Vim Easy Align](https://github.com/junegunn/vim-easy-align) plugin.
  
  - installs [Fish Vim](https://github.com/nickeb96/fish.vim) plugin. If you don't do [fish](https://fishshell.com/) scripting, you can comment out this line.
  
  - installs [fzf vim](https://github.com/junegunn/fzf.vim) plugin.
  
  - installs [Vim Polyglot](https://github.com/sheerun/vim-polyglot) plugin.
  
  - installs [Auto Pairs](https://github.com/jiangmiao/auto-pairs) plugin.
  
  - installs [You Complete Me](https://github.com/ycm-core/YouCompleteMe) plugin. For this, it needs to install `cmake`, `build-essential`, `python3-dev` and `git`.

#### For fish shell;

- put `config.fish` file to the fish configuration folder which should be located in `$HOME/.config/fish/`.
  
  ###### Properties/plugins
  
  - it uses [Starship prompt](https://github.com/starship/starship). 
    For this, it needs to install some [powerline](https://github.com/powerline/fonts) patched fonts. I use [nerd fonts](https://github.com/ryanoasis/nerd-fonts), so it downloads fonts from that GitHub page and installs. Then you need to choose the installed font manually from your terminal emulator's preferences.
  
  - it uses [exa](https://github.com/ogham/exa) as a replacement for `ls` command.
  
  - it uses [grc](https://github.com/garabik/grc) command for some colored outputs.
  
  - it uses [zoxide](https://github.com/ajeetdsouza/zoxide) directory jumper.
  
  - it uses [fzf.fish](https://github.com/PatrickF1/fzf.fish.git). For this, it needs to install `fzf`, `fd-find` and `bat`.
  
  - it uses [colored_man_pages.fish](https://github.com/PatrickF1/colored_man_pages.fish.git).
  
  - `cp` command replaced by `gcp` to have fancy progress bar.
  
  - a `pdfoptimizer` function which uses `ghostscript` added.
  
  - also it has some aliases, abbreviations and small tweaks.

#### For Starship prompt;

- put `starship.toml` file to your `$HOME/.config/` directory.
  
  - it has some fancy colored command prompt definitions.
  
  - it has `python`, `conda`, `git`, and battery indicators.

#### For kitty terminal;

- put `kitty.conf` file to `$HOME/.config/kitty/` directory.
  
  - it uses `fish` shell as default shell.
  
  - it uses the same nerd font as fish shell.
  
  - the window decorations are removed.

#### For Firefox;

- put `user-override.js` file to the Firefox's profile patch, and follow the instructions on [arkenfox](https://github.com/arkenfox/user.js/wiki/3.4-Apply-&-Update-&-Maintain).