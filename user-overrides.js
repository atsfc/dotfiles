/* override recipe: enable DRM and let me watch videos ***/
   // user_pref("media.gmp-widevinecdm.enabled", true); // 2021 default-inactive in user.js
user_pref("media.eme.enabled", true); // 2022
/* override recipe: enable session restore ***/
user_pref("browser.startup.page", 3); // 0102

user_pref("keyword.enabled", true); // 0801: disable location bar using search
user_pref("browser.formfill.enable", true); // 0810: disable search and form history
user_pref("network.http.referer.XOriginPolicy", 0); // 1601: control when to send a cross-origin referer
user_pref("privacy.clearOnShutdown.formdata", false); // 2811: set/enforce what items to clear on shutdown (if 2810 is true)
user_pref("privacy.clearOnShutdown.history", false); // 2811: set/enforce what items to clear on shutdown (if 2810 is true)
user_pref("privacy.clearOnShutdown.sessions", false); // 2811: set/enforce what items to clear on shutdown (if 2810 is true)
user_pref("privacy.clearOnShutdown.offlineApps", true); // 2811: set/enforce what items to clear on shutdown (if 2810 is true)
user_pref("privacy.clearOnShutdown.cookies", true); // 2811: set/enforce what items to clear on shutdown (if 2810 is true)
user_pref("privacy.cpd.formdata", false); // 2820:
user_pref("privacy.cpd.history", false); // 2820:
user_pref("privacy.cpd.sessions", false); // 2820:
user_pref("privacy.cpd.cookies", false); // 2820:
user_pref("privacy.resistFingerprinting.letterboxing", false); // 4504: enable RFP letterboxing [FF67+]
user_pref("browser.startup.page", 1); // 0102: set startup page [SETUP-CHROME]
user_pref("browser.startup.homepage", "about:home"); // 0103: set HOME+NEWWINDOW page
user_pref("browser.newtabpage.enabled", true); // 0104: set NEWTAB page

/* 9000 PERSONAL STUFF */
/*---------------------*/
/* 9001 firefox gnome theme requirements */
/* user.js
 * https://github.com/rafaelmardojai/firefox-gnome-theme/
 */

// Enable customChrome.css
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// Set UI density to normal
user_pref("browser.uidensity", 0);

// Enable SVG context-propertes
user_pref("svg.context-properties.content.enabled", true);

/* 2002: force WebRTC inside the proxy [FF70+] ***/
user_pref("_user.js.parrot", "2000 syntax error: the parrot's snuffed it!");

/* 2003: force a single network interface for ICE candidates generation [FF42+]
 * When using a system-wide proxy, it uses the proxy interface
 * [1] https://developer.mozilla.org/en-US/docs/Web/API/RTCIceCandidate
 * [2] https://wiki.mozilla.org/Media/WebRTC/Privacy ***/
user_pref("media.peerconnection.ice.default_address_only", true);

/* 4520: disable WebGL (Web Graphics Library)
 * [SETUP-WEB] If you need it then override it. RFP still randomizes canvas for naive scripts ***/
user_pref("webgl.disabled", true);
