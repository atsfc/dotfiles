filetype indent plugin on
syntax on
set mouse=a
set number
set clipboard=unnamedplus
" set spell spelllang=en_us

let _required_packages = ["git", "curl", "cmake", "build-essential", "python3-dev", "fzf"]

function RequiredPackage(package_name)
  silent execute '!dpkg --get-selections | grep -v ' . a:package_name . '- | grep -w ' . a:package_name . ' | grep -w install > /dev/null'
  if !v:shell_error
    return 0
  else
    silent execute '!sudo apt-get install ' . a:package_name . ' -yy > /dev/null'
    silent execute '!echo Installing required package ' . a:package_name . '.'
    if !v:shell_error
      silent execute '!echo The package ' . a:package_name . ' is istalled.'
    else
      silent execute '!echo A problem occured when installing the package ' . a:package_name . '.'
    endif
  endif
endfunction

function Requirements(...)
  for package in a:000
    call RequiredPackage(package)
  endfor
endfunction

" Control for curl, since it's needed for automatic installation of vim-plug.
if call("Requirements", _required_packages) == 0
  " An auto installer for vim-plug
  let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
  if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!echo Installing vim-plug plugin manager.'
    silent execute '!curl -sfLo ' . data_dir . '/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim > /dev/null'
    silent execute '!echo The vim-plug plugin manager is installed.'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
  call plug#begin()
  " The default plugin directory will be as follows:
  "   - Vim (Linux/macOS): '~/.vim/plugged'
  "   - Vim (Windows): '~/vimfiles/plugged'
  "   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
  " You can specify a custom plugin directory by passing it as the argument
  "   - e.g. `call plug#begin('~/.vim/plugged')`
  "   - Avoid using standard Vim directory names like 'plugin'

  " Make sure you use single quotes

  " Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
  Plug 'junegunn/vim-easy-align'

  " This is a Vim/Neovim plugin that provides Fish syntax highlighting,
  " automatic indentation as you type, file detection, and a colorscheme
  " matching Fish's default shell highlighting.
  Plug 'nickeb96/fish.vim'

  " A collection of language packs for Vim.
  Plug 'sheerun/vim-polyglot'
  
  " Kitty terminal config vim highlighting
  Plug 'fladson/vim-kitty'

  " A vim autocompletion plugin
  " build-essential, cmake, python3-dev required
  " sudo apt install build-essential cmake python3-dev
  Plug 'ycm-core/YouCompleteMe', { 'do': './install.py --clangd-completer' }

  " Add vim fzf funcionality
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'

  " Autocompletes paranthesis.
  Plug 'jiangmiao/auto-pairs'

  " Initialize plugin system
  call plug#end()
else
  silent execute '!echo There was an error when installing required packages.'
endif
